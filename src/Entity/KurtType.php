<?php

namespace Drupal\kurt\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the kurt type configuration entity.
 *
 * @ConfigEntityType(
 *   id = "kurt_type",
 *   label = @Translation("kurt type"),
 *   handlers = {
 *     "form" = {
 *       "add" = "Drupal\kurt\Form\KurtTypeForm",
 *       "edit" = "Drupal\kurt\Form\KurtTypeForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *     "list_builder" = "Drupal\kurt\KurtTypeListBuilder",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   admin_permission = "administer kurt types",
 *   bundle_of = "kurt",
 *   config_prefix = "kurt_type",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "add-form" = "/admin/structure/kurt_types/add",
 *     "edit-form" = "/admin/structure/kurt_types/manage/{kurt_type}",
 *     "delete-form" = "/admin/structure/kurt_types/manage/{kurt_type}/delete",
 *     "collection" = "/admin/structure/kurt_types"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "uuid",
 *   }
 * )
 */
class KurtType extends ConfigEntityBundleBase {

  /**
   * The machine name of this kurt type.
   *
   * @var string
   */
  protected $id;

  /**
   * The human-readable name of the kurt type.
   *
   * @var string
   */
  protected $label;

}
