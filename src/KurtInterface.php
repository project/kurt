<?php

namespace Drupal\kurt;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a kurt entity type.
 */
interface KurtInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

  /**
   * Gets the kurt creation timestamp.
   *
   * @return int
   *   Creation timestamp of the kurt.
   */
  public function getCreatedTime();

  /**
   * Sets the kurt creation timestamp.
   *
   * @param int $timestamp
   *   The kurt creation timestamp.
   *
   * @return \Drupal\kurt\KurtInterface
   *   The called kurt entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the kurt status.
   *
   * @return bool
   *   TRUE if the kurt is enabled, FALSE otherwise.
   */
  public function isEnabled();

  /**
   * Sets the kurt status.
   *
   * @param bool $status
   *   TRUE to enable this kurt, FALSE to disable.
   *
   * @return \Drupal\kurt\KurtInterface
   *   The called kurt entity.
   */
  public function setStatus($status);

}
