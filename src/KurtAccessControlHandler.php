<?php

namespace Drupal\kurt;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the access control handler for the kurt entity type.
 */
class KurtAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {

    /** @var \Drupal\kurt\KurtInterface $entity */
    $entity_type_id = $entity->getEntityTypeId();
    $bundle = $entity->bundle();
    $is_owner = $entity->getOwnerId() === $account->id();

    switch ($operation) {
      case 'view':
        if ($is_owner) {
          return AccessResult::allowedIfHasPermission($account, "view own $entity_type_id $bundle");
        }
        return AccessResult::allowedIfHasPermission($account, "view any $entity_type_id $bundle");

      case 'update':
        if ($is_owner) {
          return AccessResult::allowedIfHasPermissions($account, ["edit own $entity_type_id $bundle", 'administer kurt'], 'OR');
        }
        return AccessResult::allowedIfHasPermissions($account, ["edit any $entity_type_id $bundle", 'administer kurt'], 'OR');

      case 'delete':
        if ($is_owner) {
          return AccessResult::allowedIfHasPermissions($account, ["delete own $entity_type_id $bundle", 'administer kurt'], 'OR');
        }
        return AccessResult::allowedIfHasPermissions($account, ["delete any $entity_type_id $bundle", 'administer kurt'], 'OR');

      default:
        // No opinion.
        return AccessResult::neutral();
    }
  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL) {
    return AccessResult::allowedIfHasPermissions($account, ["create {$context['entity_type_id']} $entity_bundle", 'administer kurt'], 'OR');
  }

}
