<?php

namespace Drupal\kurt\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the kurt entity edit forms.
 */
class KurtForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    $entity = $this->getEntity();
    $result = $entity->save();
    $link = $entity->toLink($this->t('View'))->toRenderable();

    $message_arguments = ['%label' => $this->entity->label()];
    $logger_arguments = $message_arguments + ['link' => render($link)];

    if ($result == SAVED_NEW) {
      $this->messenger()->addStatus($this->t('New Kurt %label has been created.', $message_arguments));
      $this->logger('kurt')->notice('Created new Kurt %label', $logger_arguments);
    }
    else {
      $this->messenger()->addStatus($this->t('The Kurt %label has been updated.', $message_arguments));
      $this->logger('kurt')->notice('Updated new Kurt %label.', $logger_arguments);
    }

    $form_state->setRedirect('entity.kurt.canonical', ['kurt' => $entity->id()]);
  }

}
