<?php

namespace Drupal\kurt\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configuration form for a kurt entity type.
 */
class KurtSettingsForm extends FormBase {

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * {@inheritdoc}
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The Config Factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory) {
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'kurt_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('kurt.settings');

    $form['settings'] = [
      '#markup' => $this->t('Settings form for a Kurt entity type.'),
    ];

    $form['settings']['prefix'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Prefix'),
      '#description' => $this->t('Example: /lc'),
      '#default_value' => $config->get('prefix') ? $config->get('prefix') : '',
    ];

    $form['settings']['status_code'] = [
      '#type' => 'select',
      '#title' => $this->t('Status code'),
      '#description' => $this->t('You can find more information about HTTP redirect status codes at <a href="@status-codes">@status-codes</a>.', ['@status-codes' => 'http://en.wikipedia.org/wiki/List_of_HTTP_status_codes#3xx_Redirection']),
      '#default_value' => $config->get('statusCode') ? $config->get('statusCode') : '',
      '#options' => kurt_code_options(),
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory->getEditable('kurt.settings');

    if ((!empty($prefix = $form_state->getValue('prefix'))) && (!empty($statusCode = $form_state->getValue('status_code')))) {

      // Check if prefix changes.
      if ($config->get('prefix') != $prefix) {
        $this->messenger()->addWarning($this->t('The prefix has been changed, please re-generate the paths.'));
      }

      // Save prefix value.
      $config->set('prefix', $prefix)->save();
      $config->set('statusCode', $statusCode)->save();
      $this->messenger()->addStatus($this->t('The configuration has been updated.'));
    }
    else {
      $this->messenger()->addWarning($this->t('The configuration has not been updated.'));
    }
  }

}
