# Kurt

This module allows a user to generate url shortcuts with a prefix defined in the settings, or an alias lock without a prefix.  
Deadlines for availability could be set. When the shortcut is called outside these dates or is disabled, a redirection is made to the home page. Without defined dates, the shortcut is always available.

## Installation
Install the module  
(https://www.drupal.org/docs/extending-drupal/installing-modules).

## Configuration
Location: /admin/structure/kurt  
On this page, the user can choose the prefix, and the status code.  
If the status code changes, don't forget to generate paths with "bulk generate" feature. The old path will be redirect to the new path if the contrib module redirect is enabled.

## Path auto
/admin/config/search/path/patterns

## Permissions
add the view permission to anonymous users.

## Dependency


## Sponsor
* State of Geneva - https://www.ge.ch

## Maintainers
* Mickaël Silvert - https://www.drupal.org/user/yepa
